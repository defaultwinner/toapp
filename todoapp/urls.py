"""todoapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from rest_framework import routers, serializers, viewsets
from django.conf import settings
from django.conf.urls.static import static

from todolist import views as main

router = routers.DefaultRouter()
router.register(r'tasks', main.TaskViewSet)

urlpatterns = [
    re_path(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    re_path(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    re_path(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    re_path(r'^signup/$', main.signup, name='signup'),
    re_path('^$', main.Home.as_view(), name="home"),
    url(r'^todo/', include(router.urls)),
]+static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
# if settings.DEBUG:
#   urlpatterns += re_path(r'^static/(?P.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT})
# urlpatterns += 