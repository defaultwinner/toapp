from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.views.generic.base import TemplateView
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import (Task)
from .serializer import (TaskSerializer)
# Create your views here.

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})

class Home(LoginRequiredMixin, TemplateView):
	template_name = "index.html"

	def get_context_data(self, **kwargs):
		user = self.request.user
		context = super().get_context_data(**kwargs)
		return context


# @method_decorator(csrf_exempt)
class TaskViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def get_queryset(self):
        try:
            return self.request.user.task_list.all()
        except:
            raise Http404

    def list(self, request):
        queryset = self.request.user.task_list.filter(status__gte=1).order_by('status','updated_at')
        serializer = TaskSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        request.POST._mutable = True
        request.data['user'] = self.request.user.id
        request.POST._mutable = False
        response = super().create(request)
        return response

    def delete(self, request):
        request.POST._mutable = True
        request.data['user'] = self.request.user.id
        request.POST._mutable = False
        response = super().delete(request)
        return response

    def perform_update(self, serializer):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=self.request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
	        # return response