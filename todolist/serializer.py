""" Serializer for Kitchen  Models."""
import datetime
from django.conf.urls import url, include
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework import routers, serializers, viewsets
import todolist.models as listmodels


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = listmodels.Task
        fields= '__all__'
