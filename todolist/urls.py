from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets

from . import views
orderrouter = routers.DefaultRouter()
app_name = 'orders'
orderrouter.register(r'orders', views.OrderViewSet, 'OrderDetails')
urlpatterns = [
    # url(r'^$', views.IndexView.as_view(), name='index'),
    # url(r'^(?P<pk>\d+)/$', views.DetailView.as_view(), name='detail'),
    url(r'', include(orderrouter.urls, namespace='orders')),
]