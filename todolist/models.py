from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils.text import slugify

from django.contrib.auth.models import User
import datetime

# Create your models here.


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Task(BaseModel):
    PENDING = 1
    CANCELLED = 0
    COMPLETED = 2
    TASK_STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (COMPLETED, 'Completed'),
        (CANCELLED, 'Cancelled')
    )
    user = models.ForeignKey(
            User, null = False,
            related_name = 'task_list',
            on_delete = models.CASCADE
        )
    title = models.CharField(max_length = 200)
    description = models.TextField(null = True, blank = True)
    status = models.PositiveIntegerField(
        choices = TASK_STATUS_CHOICES, default = PENDING)

    def __str__(self):
        return "{}".format(self.title)
