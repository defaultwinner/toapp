FROM python:3

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get install -y netcat
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
COPY ./entrypoint.sh /code/
ENV POSTGRES_USER houzenuser
ENV POSTGRES_PASSWORD houzenpwd
ENV POSTGRES_DB houzendb

ENTRYPOINT ["/code/entrypoint.sh"]